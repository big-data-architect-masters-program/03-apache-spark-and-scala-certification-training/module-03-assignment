# Assignment edureka: 
### Module 3 - OOPS and Functional Programming in Scala
* [x] 1\. Write a class AccountInfo with methods deposit and withdraw, and a read-only property balance. [here](https://gitlab.com/edureka-big-data-architect/apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/AccountInfo.scala)
* [x] 2\. Write an object Conversions with methods inchestoFeet, milestoKms and poundsToKilos and invoke its methods from a class of your choice. [here](https://gitlab.com/edureka-big-data-architect/apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/Conversions.scala)
* [x] 3\. Extend the following BankAccount class to a CheckingAccount class that charges $1 for every deposit and withdrawal. [here](https://gitlab.com/big-data-architect-masters-program/03-apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/BankAccount.scala)
```scala
class BankAccount(initBal:Double){
  private var balance = initBal
  def deposit(amount:Double)={
    balance += amount;
    balance
  }
  def withdraw(amount:Double)={
    balance -= amount;
    balance
  }
}
```
* [x] 4\. Write a Scala program to get the largest element of an array using reduceLeft. [here](https://gitlab.com/big-data-architect-masters-program/03-apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/Statement_04.scala)
* [x] 5\. Implement the factorial function using to and reduceLeft, without a loop or recursion. [here](https://gitlab.com/big-data-architect-masters-program/03-apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/Statement_05.scala)
* [x] 6\. Write a Scala code which reverses the lines of a file (makes the first line as the last one, and so on). [here](https://gitlab.com/big-data-architect-masters-program/03-apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/Statement_06.scala)
* [x] 7\. Write a Scala code which reads a file and prints all words with more than 10 characters. Can you write all of it in a single line? [here](https://gitlab.com/big-data-architect-masters-program/03-apache-spark-and-scala-certification-training/module-03-assignment/blob/master/src/Statement_07.scala)