object Statement_05 extends App {
  def factX(a:BigInt):BigInt={
    if (a <= 1){
       1
    }else{
      a * factX(a-1)
    }
  }
  println(factX(3))
}
