class Conversion{
  def inchesToFeet(i:Double)={
    println(i +" Inches = " + (i * 0.0833333) + " Foot")
  }
  def milesToKms(m:Double)={
    println(m +" Miles = " + (m * 1.60934) + " Kms")
  }
  def poundsToKilos(p:Double)={
    println(p +" Pounds = " + (p * 0.453592) + " Kgs")
  }
}

object Conversions extends App {
  var c = new Conversion
  c.inchesToFeet(0.5)
  c.milesToKms(2)
  c.poundsToKilos(3)
}
