class BankAccount(initBal: Double) {
  private var balance = initBal

  def deposit(amount: Double) = {
    balance += amount
    balance
  }

  def withdraw(amount: Double) = {
    balance -= amount
    balance
  }

}

class CheckingAccount(initBal: Double) extends BankAccount(initBal){

  val bankCharge = 1.0

  override def deposit(amount: Double): Double = super.deposit(amount) - bankCharge

  override def withdraw(amount: Double): Double = super.withdraw(amount) - bankCharge
}

object Main extends App{
  var a = new CheckingAccount(5000)
  println(a.deposit(100))
  println(a.withdraw(100))
}