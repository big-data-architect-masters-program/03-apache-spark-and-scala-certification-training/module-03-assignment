object Statement_04 extends App {
  val donutPrices = Array(1.5, 2.0, 2.5)
  println(s"Highest donut price = ${donutPrices.reduceLeft(_ max _)}")
}
