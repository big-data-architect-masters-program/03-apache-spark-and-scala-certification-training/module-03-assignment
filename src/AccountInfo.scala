class AccountInfo {
  private var _balance = 0

  def deposit(amn: Int) = {
    _balance += amn
  }

  def currentBalance()={println(_balance)}

  def withdraw(amn:Int)={
    if (_balance >= amn){
      _balance -= amn
    }else{
      println("You don't have enough balance.")
    }
  }
}

object MyAccountInfo extends App {

  var a = new AccountInfo
  a.currentBalance()
  a.deposit(50)
  a.currentBalance()
  a.withdraw(30)
  a.currentBalance()
  a.withdraw(50)
  a.currentBalance()
}
